package ir.arbn.www.mysematecprojecteight;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by A.R.B.N on 2/13/2018.
 */

public class StudentsDbHandler extends SQLiteOpenHelper {
    String tblCreate = "" + "CREATE TABLE students (" + "id INTEGER PRIMARY KEY AUTOINCREMENT," + "username TEXT," + "password TEXT," + "mobile TEXT," + ")";

    public StudentsDbHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public void insertStudent(String username, String password, String mobile) {
        String insertQuery = "" +
                "INSERT INTO students(username, password, mobile)" +
                "VALUES ('" + username + "','" + password + "','" + mobile + "')";
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(insertQuery);
        db.close();
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(tblCreate);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
